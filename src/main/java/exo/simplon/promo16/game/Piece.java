package exo.simplon.promo16.game;

import exo.simplon.promo16.game.Game.Type;

public class Piece {
    int column;
    int row;
    boolean isWhite;
    Type type;

    public Piece(int column, int row, boolean isWhite, Type type) {
        this.column = column;
        this.row = row;
        this.isWhite = isWhite;
        this.type = type;
    }
}
