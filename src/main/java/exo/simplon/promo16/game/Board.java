package exo.simplon.promo16.game;

import java.util.ArrayList;
import java.util.List;

import exo.simplon.promo16.game.Game.Type;

public class Board {
    private List<Piece> pieces = new ArrayList<>();

    public Board(List<Piece> pieces) {
        this.pieces.add(new Piece(3, 7, true, Type.Q));
        this.pieces.add(new Piece(3, 0, false, Type.Q));
        this.pieces.add(new Piece(4, 7, true, Type.K));
        this.pieces.add(new Piece(4, 0, false, Type.K));
        for (int i = 0; i < 2; i++) {
            this.pieces.add(new Piece(2 + i * 3, 7, true, Type.B));
            this.pieces.add(new Piece(2 + i * 3, 0, false, Type.B));
            this.pieces.add(new Piece(0 + i * 7, 7, true, Type.R));
            this.pieces.add(new Piece(0 + i * 7, 0, false, Type.R));
            this.pieces.add(new Piece(1 + i * 5, 7, true, Type.N));
            this.pieces.add(new Piece(1 + i * 5, 0, false, Type.N));
        }

        for (int i = 0; i < 8; i++) {
            this.pieces.add(new Piece(i, 6, true, Type.P));
            this.pieces.add(new Piece(i, 1, false, Type.P));
        }
    }

    public String initBoard() {
        String board = "   a  b  c  d  e  f  g  h\n";
        for (int i = 0; i < 8; i++) {
            board += 8 - i;
            for (int j = 0; j < 8; j++) {
                Piece piece = piecePosition(j, i);
                board += " ";
                if (piece == null) {
                    board += " .";
                } else {
                    switch (piece.type) {
                        case P:
                            board += piece.isWhite ? " ♟" : " ♙";
                            break;
                        case N:
                            board += piece.isWhite ? " ♞" : " ♘";
                            break;
                        case B:
                            board += piece.isWhite ? " ♝" : " ♗";
                            break;
                        case K:
                            board += piece.isWhite ? " ♚" : " ♔";
                            break;
                        case R:
                            board += piece.isWhite ? " ♜" : " ♖";
                            break;
                        case Q:
                            board += piece.isWhite ? " ♛" : " ♕";
                            break;
                    }
                }
            }
            board += " ";
            board += 8 - i;
            board += "\n";
        }
        board += "   a  b  c  d  e  f  g  h\n";
        return board;
    }

    Piece piecePosition(int column, int row) {
        for (Piece piece : pieces) {
            if (piece.column == column && piece.row == row) {
                return piece;
            }
        }
        return null;
    }

}
