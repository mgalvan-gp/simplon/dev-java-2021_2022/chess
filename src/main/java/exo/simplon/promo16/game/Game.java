package exo.simplon.promo16.game;

public class Game {
    public static void main(String[] args) {
        Board board = new Board(null);
        System.out.println(board.initBoard());
    }

    enum Type {
        P, // pion
        R, // tour
        N, // cavalier
        B, // fou
        Q, // dame
        K, // roi
    }
}
